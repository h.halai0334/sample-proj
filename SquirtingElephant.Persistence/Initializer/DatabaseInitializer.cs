using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using SquirtingElephant.Common.Constants;
using SquirtingElephant.Domain.Entities;
using SquirtingElephant.Persistence.Context;

namespace SquirtingElephant.Persistence.Initializer
{
    public class DatabaseInitializer
    {
        public static void Initialize(ApplicationDbContext context)
        {
            DatabaseInitializer initializer = new DatabaseInitializer();
            initializer.SeedEverything(context);
        }

        private void SeedEverything(ApplicationDbContext context)
        {
            SeedStoredProcedures(context);
            SeedRoles(context);
            SeedUsers(context);
        }


        public void SeedStoredProcedures(ApplicationDbContext context)
        {
            //Adding Wallet Update Trigger
            // context.Database.ExecuteSqlRaw(@"
            //     DROP TRIGGER IF EXISTS walletUpdate;
            //     CREATE TRIGGER walletUpdate
            //         AFTER INSERT
            //         ON UserWallets FOR EACH ROW
            //        INSERT INTO UserBalances (UserId, Balance) VALUES (New.UserId, New.Amount) ON DUPLICATE KEY UPDATE Balance= (Select Sum(uw.Amount) From UserWallets as uw Where uw.UserId = UserId)
            //  ");
        }

        private void SeedRoles(ApplicationDbContext context)
        {
            if (context.Roles.Any())
            {
                return;
            }

            string[] roles = new[]
                {RoleNames.Admin, RoleNames.User};

            foreach (var role in roles)
            {
                context.Roles.Add(new Role()
                {
                    Name = role,
                    NormalizedName = role.ToUpper(),
                });
            }

            context.SaveChanges();
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            if (context.Users.Any())
            {
                return;
            }


            var admin = GetUser(context, "Admin", RoleNames.Admin, Constant.AdminPhone, Constant.AdminEmail,
                Constant.AdminPassword);
            context.Add(admin);
            context.SaveChanges();
        }

        private User GetUser(ApplicationDbContext context, string name, string role, string phoneNumber, string email,
            string password)
        {
            string adminRoleId = context.Roles.First(p => p.Name == role).Id;
            var admin = new User()
            {
                PhoneNumber = phoneNumber,
                Email = email,
                IsEnabled = true,
                NormalizedEmail = email.ToUpper(),
                FullName = name,
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };
            admin.UserRoles = new List<UserRole>()
            {
                new UserRole()
                {
                    RoleId = adminRoleId,
                    UserId = admin.Id
                }
            };
            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            admin.PasswordHash = passwordHasher.HashPassword(admin, password);
            return admin;
        }
    }
}