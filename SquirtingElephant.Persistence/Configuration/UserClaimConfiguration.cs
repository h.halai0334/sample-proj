using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SquirtingElephant.Domain.Entities;

namespace SquirtingElephant.Persistence.Configuration
{
    public class UserClaimConfiguration : IEntityTypeConfiguration<UserClaim>
    {
        public void Configure(EntityTypeBuilder<UserClaim> builder)
        {
            builder.HasOne(p => p.User)
                .WithMany(p => p.UserClaims)
                .HasForeignKey(p => p.UserId);
        }
    }
}