﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SquirtingElephant.Domain.Entities;

namespace SquirtingElephant.Persistence.Configuration
{
    public class ImageHolderConfiguration : IEntityTypeConfiguration<ImageHolder>
    {
        public void Configure(EntityTypeBuilder<ImageHolder> builder)
        {
            builder.Property(p => p.Image).IsRequired();
        }
    }
}