using System;
using System.IO;
using Microsoft.Extensions.Logging;
using SquirtingElephant.Application.Interfaces;

namespace SquirtingElephant.Infrastructure.Service
{
    public class ImageService : IImageService
    {
        private readonly ILogger<ImageService> _logger;

        public ImageService(ILogger<ImageService> logger)
        {
            _logger = logger;
        }

        public string SaveImage(string base64, string extension = ".jpg", string targetFolder = "img")
        {
            try
            {
                var separator = Path.DirectorySeparatorChar;
                if (string.IsNullOrWhiteSpace(targetFolder))
                {
                    targetFolder = "img";
                }
                else
                {
                    targetFolder = $"img{separator}{targetFolder}";
                }
                byte[] image = LoadImage(base64);
                string fileName = Guid.NewGuid() + extension;

                string directoryPath = Path.Combine(
                    Directory.GetCurrentDirectory(), $"wwwroot{separator}{targetFolder}");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
                string path = Path.Combine(directoryPath,fileName);
            
                using (FileStream stream = new FileStream(path, FileMode.Create))
                {
                    stream.Write(image, 0, image.Length);
                    return $"/{targetFolder}/" + fileName;
                }
            }
            catch (Exception e)
            {

                _logger.LogError(e,"ImageError");
            }
            return null;

        }

        public (string, string) SaveImageWithResize(string base64, string extension = ".jpg", string targetFolder = "img")
        {
            try
            {
                var separator = Path.DirectorySeparatorChar;
                if (string.IsNullOrWhiteSpace(targetFolder))
                {
                    targetFolder = "img";
                }
                else
                {
                    targetFolder = $"img{separator}{targetFolder}";
                }
                byte[] image = LoadImage(base64);
                string fileName = Guid.NewGuid() + extension;
                string smallFileName = Guid.NewGuid() + extension;
                string directoryPath = Path.Combine(
                    Directory.GetCurrentDirectory(), $"wwwroot{separator}{targetFolder}");
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
                string path = Path.Combine(directoryPath,fileName);
                string smallPath = Path.Combine(directoryPath,smallFileName);
                using (FileStream stream = new FileStream(path, FileMode.Create))
                {
                    stream.Write(image, 0, image.Length);
                }
                var originalImage = $"/{targetFolder}/" + fileName;
                var smallImage = $"/{targetFolder}/" + smallFileName;
                var result = ResizeImage(path, smallPath, 64);
                return (originalImage, smallImage);
            }
            catch (Exception e)
            {

                _logger.LogError(e,"ImageError");
            }
            return (null,null);
        }

        public void DeleteImage(string url)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(url))
                {
                    return;
                }
                if (File.Exists("wwwroot" + url))
                {
                    File.Delete("wwwroot" + url);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }
        }

        byte[] LoadImage(string baseString)
        {
            baseString = baseString.Remove(0, baseString.IndexOf(',') + 1);
            return Convert.FromBase64String(baseString);
        }
        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="sourcePath">The Path of image to resize.</param>
        /// <param name="pathToSave">The Path of image to save to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>The resized image.</returns>
        public string ResizeImage(string sourcePath,string pathToSave, int height)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Resize the image to the specified width and height.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="pathToSave">The Path of image to resize.</param>
        /// <param name="width">The width to resize to.</param>
        /// <param name="height">The height to resize to.</param>
        /// <returns>Saved Path of Image</returns>
        /// <exception cref="Exception">Image doesnt exist or issue in stream</exception>
        public string ResizeImage(Stream stream, string pathToSave, int width, int height)
        {
            throw new NotImplementedException();
        }
    }
}