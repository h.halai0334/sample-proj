using Microsoft.AspNetCore.Identity;

namespace SquirtingElephant.Domain.Entities
{
    public class UserClaim : IdentityUserClaim<string>
    {
        public User User { get; set; }    
    }
}