using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using SquirtingElephant.Domain.Interfaces;

namespace SquirtingElephant.Domain.Entities
{
    public class User: IdentityUser, IBase
    {
        public User()
        {
        }
        public string FullName { get; set; }
        public string FcmId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        /// <summary>
        /// Navigation property for the roles this user belongs to.
        /// </summary>
        public virtual ICollection<UserRole> UserRoles { get; set; }

        /// <summary>
        /// Navigation property for the claims this user possesses.
        /// </summary>
        public virtual ICollection<UserClaim> UserClaims { get; set; }

        public bool IsEnabled { get; set; }
    }
}