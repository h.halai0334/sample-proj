using Microsoft.AspNetCore.Identity;

namespace SquirtingElephant.Domain.Entities
{
    public class RoleClaim : IdentityRoleClaim<string>
    {
        public Role Role { get; set; }        
    }
}