﻿namespace SquirtingElephant.Domain.Entities
{
    public class ImageHolder : Base
    {
        public string Image { get; set; }
    }
}

// CQRS
// Command Query Responsibility Segregation