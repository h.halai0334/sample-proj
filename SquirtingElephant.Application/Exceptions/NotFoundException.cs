using System;

namespace SquirtingElephant.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message) : base(message)
        {
        }

        public NotFoundException()
            : base($"Resource was not found")
        {
        }

        public NotFoundException(string name, object key)
            : base($"Resource \"{name}\" was not found")
        {
        }
    }
}