using FluentValidation;
using MediatR;
using SquirtingElephant.Application.Extensions;
using SquirtingElephant.Common.Requests;
using SquirtingElephant.Common.Response;

namespace SquirtingElephant.Application.Shared
{
    public abstract class GetPagedRequest<T> : GetPageModel,IRequest<T>
    {
        
    }
    public abstract class GetPagedRequest: GetPageModel,  IRequest<ResponseViewModel>
    {
    }

    public abstract class GetUserPagedRequest : GetPagedRequest
    {
        public string UserId { get; set; }
    }
    public abstract class GetUserPagedRequest<T> : GetPagedRequest<T>
    {
        public string UserId { get; set; }
    }

    public class PageRequestValidator<T> : AbstractValidator<T> where T : GetPageModel
    {
        public PageRequestValidator()
        {
            RuleFor(p => p.Page).Required();
            RuleFor(p => p.PageSize).Max(20);
        }
        
    }
    
    
    public class UserPageRequestValidator<T> : PageRequestValidator<T> where T : GetUserPagedRequest
    {
        public UserPageRequestValidator()
        {
            RuleFor(p => p.UserId).Required();
        }
    }

    public class DeleteIdRequest : IRequest<ResponseViewModel>
    {
        public int Id { get; set; }
    }

    public class DeleteIdValidator<T> : AbstractValidator<T> where T : DeleteIdRequest
    {
        public DeleteIdValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    } 
 
}