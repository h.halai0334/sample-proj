using System.Threading.Tasks;

namespace SquirtingElephant.Application.Interfaces
{
    public interface IAlertService
    {
        Task<bool> SendAlertToAdmin(string message);
    }
}