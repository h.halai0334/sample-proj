using System.Threading.Tasks;

namespace SquirtingElephant.Application.Interfaces
{
    public interface ISmsService
    {
        Task<bool> SendMessage(string phoneNumber, string message);
    }
}