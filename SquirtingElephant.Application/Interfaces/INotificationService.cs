using System.Threading.Tasks;

namespace SquirtingElephant.Application.Interfaces
{
    public interface INotificationService
    {
        Task<bool> SendNotification(string fcmId, string title, string body);
        Task<bool> SendNotification(string fcmId, string title, object body);
    }
}