using System.Threading.Tasks;

namespace SquirtingElephant.Application.Interfaces
{
    public interface IEmailService
    {
        Task<bool> SendEmail(string email, string subject, string body);
        Task<bool> SendEmail(string fromEmail, string fromName, string toEmail, string subject, string body);
        Task<bool> SendEmailImageAttachment(string email, string subject, string body, string filePath, string fileName, string fileType);
        Task<bool> SendEmailImageAttachment(string fromEmail, string fromName, string toEmail, string subject, string body, string filePath, string fileName, string fileType);
    }
}