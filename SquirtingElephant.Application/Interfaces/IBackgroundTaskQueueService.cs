﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace SquirtingElephant.Application.Interfaces
{
    public interface IBackgroundTaskQueueService
    {
        void QueueBackgroundWorkItem(IRequest<object> workItem);
        void QueueBackgroundWorkItem(IRequest<object> workItem, CancellationToken token);

        Task<IRequest<object>> DequeueAsync(
            CancellationToken cancellationToken);
    }
}