using System.IO;

namespace SquirtingElephant.Application.Interfaces
{
    public interface IImageService
    {
        string SaveImage(string base64, string extension = ".jpg", string targetFolder = "img");
        (string,string) SaveImageWithResize(string base64, string extension = ".jpg", string targetFolder = "img");
        void DeleteImage(string url);
        string ResizeImage(string sourcePath,string pathToSave, int height);
        string ResizeImage(Stream stream, string pathToSave, int width, int height);
    }
}