using System.Threading.Tasks;

namespace SquirtingElephant.Application.Interfaces
{
    public interface IChatService
    {
        Task<bool> SendMessageToUser(string userId, object message);
    }
}