﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.EntityFrameworkCore;
using SquirtingElephant.Application.Images.Models;
using SquirtingElephant.Application.Shared;
using SquirtingElephant.Persistence.Context;
using SquirtingElephant.Persistence.Extension;

namespace SquirtingElephant.Application.Images.Queries.GetImages
{
    public class GetImagesRequestModel : GetPagedRequest<GetImagesResponseModel>
    {
        
    }

    public class GetImagesRequestModelValidator : PageRequestValidator<GetImagesRequestModel>
    {
        public GetImagesRequestModelValidator()
        {
        }
    }

    public class GetImagesRequestHandler : IRequestHandler<GetImagesRequestModel, GetImagesResponseModel>
    {
        private readonly ApplicationDbContext _context;

        public GetImagesRequestHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<GetImagesResponseModel> Handle(GetImagesRequestModel request, CancellationToken cancellationToken)
        {
            request.SetDefaultValue();
            var list = await _context.ImageHolders.GetManyReadOnly(request)
                .Select(ImageSelector.Selector)
                .ToListAsync(cancellationToken);
            var count = await _context.ImageHolders.CountAsync(cancellationToken);
            return new GetImagesResponseModel()
            {
                Data = list,
                Count = count
            };
        }
    }

    public class GetImagesResponseModel
    {
        public List<ImageDto> Data { get; set; }
        public int Count { get; set; }
    }
}