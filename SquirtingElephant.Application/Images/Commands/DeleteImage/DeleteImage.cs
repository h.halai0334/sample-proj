﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SquirtingElephant.Application.Exceptions;
using SquirtingElephant.Application.Extensions;
using SquirtingElephant.Application.Interfaces;
using SquirtingElephant.Persistence.Context;
using SquirtingElephant.Persistence.Extension;

namespace SquirtingElephant.Application.Images.Commands.DeleteImage
{
    public class DeleteImageRequestModel : IRequest<DeleteImageResponseModel>
    {
        public int Id { get; set; }
    }

    public class DeleteImageRequestModelValidator : AbstractValidator<DeleteImageRequestModel>
    {
        public DeleteImageRequestModelValidator()
        {
            RuleFor(p => p.Id).Required();
        }
    }

    public class DeleteImageRequestHandler : IRequestHandler<DeleteImageRequestModel, DeleteImageResponseModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public DeleteImageRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<DeleteImageResponseModel> Handle(DeleteImageRequestModel request, CancellationToken cancellationToken)
        {
            var image = await _context.ImageHolders.GetByAsync(p => p.Id == request.Id, cancellationToken: cancellationToken);
            if (image == null)
            {
                throw new NotFoundException();
            }

            _context.Remove(image);
            await _context.SaveChangesAsync(cancellationToken);
            _imageService.DeleteImage(image.Image);
            return new DeleteImageResponseModel();
        }
    }

    public class DeleteImageResponseModel
    {
    }
}