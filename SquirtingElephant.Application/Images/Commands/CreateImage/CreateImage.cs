﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MediatR;
using SquirtingElephant.Application.Extensions;
using SquirtingElephant.Application.Images.Models;
using SquirtingElephant.Application.Interfaces;
using SquirtingElephant.Domain.Entities;
using SquirtingElephant.Persistence.Context;

namespace SquirtingElephant.Application.Images.Commands.CreateImage
{
    public class CreateImageRequestModel : IRequest<CreateImageResponseModel>
    {
        public string Image { get; set; }
    }

    public class CreateImageRequestModelValidator : AbstractValidator<CreateImageRequestModel>
    {
        public CreateImageRequestModelValidator()
        {
            RuleFor(p => p.Image).Required();
        }
    }

    public class CreateImageRequestHandler : IRequestHandler<CreateImageRequestModel, CreateImageResponseModel>
    {
        private readonly ApplicationDbContext _context;
        private readonly IImageService _imageService;
        public CreateImageRequestHandler(ApplicationDbContext context, IImageService imageService)
        {
            _context = context;
            _imageService = imageService;
        }

        public async Task<CreateImageResponseModel> Handle(CreateImageRequestModel request, CancellationToken cancellationToken)
        {
            var image = _imageService.SaveImage(request.Image);
            var imageHolder = new ImageHolder()
            {
                Image = image
            };
            await _context.ImageHolders.AddAsync(imageHolder, cancellationToken);
            _context.SaveChanges();
            return new CreateImageResponseModel(imageHolder);
        }
    }

    public class CreateImageResponseModel : ImageDto
    {
        public CreateImageResponseModel(ImageHolder holder): base(holder)
        {
            
        }
    }
}