﻿using System;
using System.Linq.Expressions;
using SquirtingElephant.Common.Extensions;
using SquirtingElephant.Domain.Entities;

namespace SquirtingElephant.Application.Images.Models
{
    public class ImageDto
    {
        public ImageDto()
        {
            
        }

        public ImageDto(ImageHolder imageHolder)
        {
            if (imageHolder == null)
            {
                throw new ArgumentNullException(nameof(imageHolder));
            }
            Id = imageHolder.Id;
            Image = imageHolder.Image;
            CreatedDate = imageHolder.CreatedDate.ToGeneralDateTime();
        }
        public int Id { get; set; }
        public string Image { get; set; }
        public string CreatedDate { get; set; }
    }

    public static class ImageSelector
    {
        public static Expression<Func<ImageHolder,ImageDto>> Selector = p => new ImageDto()
        {
            Id = p.Id,
            Image = p.Image,
            CreatedDate = p.CreatedDate.ToGeneralDateTime()
        };
    }
}