using FluentValidation.Validators;

namespace SquirtingElephant.Application.CustomValidator
{
    public class ImeiNumberValidator : PropertyValidator
    {
        public ImeiNumberValidator(string errorMessage) : base(errorMessage)
        {
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            return context.PropertyValue.ToString().Length == 15;
        }
    }
}