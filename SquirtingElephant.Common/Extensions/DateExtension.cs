using System;

namespace SquirtingElephant.Common.Extensions
{
    public static class DateExtension
    {
        public static string ToGeneralDateTime(this long ticks)
        {
            if (ticks == 0)
            {
                return "";
            }
            DateTime myDate = new DateTime(ticks);
            return myDate.ToGeneralDateTime();
        }
        public static string ToDayAndDate(this DateTime dateTime)
        {
            return dateTime.ToString("ddd dd-MMM");
        }
        public static string ToShortDate(this DateTime dateTime)
        {
            return dateTime.ToShortDateString();
        }
        public static string ToGeneralDateTime(this DateTime dateTime)
        {
            return dateTime.ToString("u");
        }
        public static string ToGeneralDateTime(this DateTime? dateTime)
        {
            return dateTime.GetValueOrDefault().ToString("u");
        }

        public static string ToUtc(this DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToGeneralDateTime();
        }
        
    }
}