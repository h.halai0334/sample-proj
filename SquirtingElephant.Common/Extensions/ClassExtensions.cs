namespace SquirtingElephant.Common.Extensions
{
    public static class ClassExtensions
    {
        /// <summary>
        /// This functions copies all matching fields into the parameter child
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="child"></param>
        public static void Copy(this object parent, object child)
        {
            var parentProperties = parent.GetType().GetProperties();
            var childProperties = child.GetType().GetProperties();

            foreach (var parentProperty in parentProperties)
            {
                foreach (var childProperty in childProperties)
                {
                    if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                    {
                        childProperty.SetValue(child, parentProperty.GetValue(parent));
                        break;
                    }
                }
            }
        }
    }
}