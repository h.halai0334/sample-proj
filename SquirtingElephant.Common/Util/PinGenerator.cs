using System;
using System.Linq;

namespace SquirtingElephant.Common.Util
{
    public class PinGenerator
    {
        public static string CreatePin()
        {
            return new Random(DateTime.UtcNow.Millisecond).Next(1000,9999) + "";
        }
        
    }

    public class BookingNumberGenerator
    {
        public static string Generate()
        {
            return "BK-" + StringGenerator.RandomString(6) + "-" + DateTime.UtcNow.ToString("hh-mm-ss");
        }
        
    }
    public class StringGenerator
    {
        const string Chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        public static string RandomString(int length)
        {
            var random = new Random((int)DateTime.UtcNow.Ticks);
            return new string(Enumerable.Repeat(Chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string Password()
        {
            return "Tam@12345678";
        }

        public static string GuidString()
        {
            var guid = Guid.NewGuid().ToString().Replace("-", "");
            return guid;
        }
    }
}