namespace SquirtingElephant.Common.Constants
{
    public class RoleNames
    {
        public const string Admin = "Administrator";
        public const string User = "User";
    }
}