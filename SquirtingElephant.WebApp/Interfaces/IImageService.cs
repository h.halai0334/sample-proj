﻿using System.Threading.Tasks;
using SquirtingElephant.WebApp.Models.Dto;
using SquirtingElephant.WebApp.Models.Request;
using SquirtingElephant.WebApp.Models.Response;

namespace SquirtingElephant.WebApp.Interfaces
{
    public interface IImageService
    {
        Task<(GetImagesResponse, ErrorResponse)> GetImages(GetPageRequest request);
        Task<(ImageDto, ErrorResponse)> CreateImage(CreateImageRequest request);
        Task<(ImageDto, ErrorResponse)> DeleteImage(DeleteImageRequest request);
    }

    public interface ILoginService
    {
        Task<(LoginResponse, ErrorResponse)> Login(LoginRequest request);
    }
}