﻿namespace SquirtingElephant.WebApp.Models.Dto
{
    public class ImageDto
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string CreatedDate { get; set; }
    }
}