﻿using System.Collections.Generic;

namespace SquirtingElephant.WebApp.Models.Response
{
    public class ErrorResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<string> Errors { get; set; }
    }
}