﻿using System.Collections.Generic;
using SquirtingElephant.WebApp.Models.Dto;

namespace SquirtingElephant.WebApp.Models.Response
{
    public class GetImagesResponse
    {
        public List<ImageDto> Data { get; set; }
        public int Count { get; set; }
    }
    public class LoginResponse
    {
        public string token_type { get; set; }
        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string id_token { get; set; }
    }
}