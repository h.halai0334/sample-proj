﻿namespace SquirtingElephant.WebApp.Models.Request
{
    public class LoginRequest
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class DeleteImageRequest
    {
        public int Id { get; set; }
    }
    public class CreateImageRequest
    {
        public string Image { get; set; }
        public string Token { get; set; }
    }
    public class GetPageRequest
    {
        public string Token { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}