﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SquirtingElephant.WebApp.Interfaces;
using SquirtingElephant.WebApp.Models.Dto;
using SquirtingElephant.WebApp.Models.Request;
using SquirtingElephant.WebApp.Models.Response;

namespace SquirtingElephant.WebApp.Services
{
    public class LoginService : ILoginService
    {
        private readonly HttpClient _client;

        public LoginService(HttpClient client)
        {
            _client = client;
        }

        public async Task<(LoginResponse,ErrorResponse)> Login(LoginRequest request)
        {
            var body = new Dictionary<string, string>
            {
                {"username", request.Email},
                {"password", request.Password},
                {"granttype", "password"},
                {"grant_type", "password"},
                {"scope", "openid email profile offline_access roles"}
            };

            var response = await _client.PostAsync("/connect/token", new FormUrlEncodedContent(body));
            var respondeBody = await response.Content.ReadAsStringAsync();
             if (response.IsSuccessStatusCode)
             {
                 var result = JsonConvert.DeserializeObject<LoginResponse>(respondeBody);
                 return (result, null);
             }
             var error = JsonConvert.DeserializeObject<ErrorResponse>(respondeBody);
             return (null, error);
        }
    }
    public class ImageService : IImageService
    {
        private readonly HttpClient _client;
        private const string Url = "/api/v1/image";
        public ImageService(HttpClient client)
        {
            _client = client;
        }

        public async Task<(GetImagesResponse, ErrorResponse)> GetImages(GetPageRequest request)
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", request.Token);
            var response = await _client.GetAsync(Url + "?page=" + request.Page + "&pageSize=" + request.PageSize);
            if (response.IsSuccessStatusCode)
            {
                return (JsonConvert.DeserializeObject<GetImagesResponse>(await response.Content.ReadAsStringAsync()),
                    null);
            }
            return (null, JsonConvert.DeserializeObject<ErrorResponse>(await response.Content.ReadAsStringAsync()));
        }

        public async Task<(ImageDto, ErrorResponse)> CreateImage(CreateImageRequest request)
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", request.Token);
            var response = await _client.PostAsJsonAsync(Url,request);
            if (response.IsSuccessStatusCode)
            {
                return (JsonConvert.DeserializeObject<ImageDto>(await response.Content.ReadAsStringAsync()),
                    null);
            }
            return (null, JsonConvert.DeserializeObject<ErrorResponse>(await response.Content.ReadAsStringAsync()));
        }

        public async Task<(ImageDto, ErrorResponse)> DeleteImage(DeleteImageRequest request)
        {
            var response = await _client.DeleteAsync(Url + "/" + request.Id);
            if (response.IsSuccessStatusCode)
            {
                return (JsonConvert.DeserializeObject<ImageDto>(await response.Content.ReadAsStringAsync()),
                    null);
            }
            return (null, JsonConvert.DeserializeObject<ErrorResponse>(await response.Content.ReadAsStringAsync()));

        }
    }
}