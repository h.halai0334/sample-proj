﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SquirtingElephant.Application.Images.Commands.CreateImage;
using SquirtingElephant.Application.Images.Commands.DeleteImage;
using SquirtingElephant.Application.Images.Queries.GetImages;

namespace SquirtingElephant.WebApi.Controllers.Mobile.V1
{
    public class ImageController : BaseController
    {
        ///<summary>
        /// Get Image
        ///</summary>
        [Authorize]
        [HttpGet]
        public async Task<GetImagesResponseModel> GetImage([FromQuery] GetImagesRequestModel model, CancellationToken token)
        {
            return await Mediator.Send(model, token);
        }



        ///<summary>
        /// Create Image
        ///</summary>
        [Authorize]
        [HttpPost]
        public async Task<CreateImageResponseModel> CreateImage(CreateImageRequestModel model, CancellationToken token)
        {
            return await Mediator.Send(model, token);
        }




        ///<summary>
        /// Delete Image
        ///</summary>
        [Authorize]
        [HttpDelete("{id}")]
        public async Task<DeleteImageResponseModel> DeleteImage([FromRoute] int id, CancellationToken token)
        {
            var model = new DeleteImageRequestModel();
            model.Id = id;
            return await Mediator.Send(model, token);
        }
    }
}