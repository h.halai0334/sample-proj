using SquirtingElephant.Common.Constants;
using System.Security.Claims;

namespace SquirtingElephant.WebApi.Extension
{
    public static class ClaimPrincipleExtension
    {
        public static string GetUserId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirstValue(CustomClaimTypes.UserId);
        }
        
        public static int GetCompanyId(this ClaimsPrincipal claimsPrincipal)
        {
            var id = claimsPrincipal.FindFirstValue(CustomClaimTypes.CompanyId);
            int.TryParse(id, out var companyId);
            return companyId;
        }

        public static string GetRole(this ClaimsPrincipal claimsPrincipal)
        {
            if (claimsPrincipal.IsInRole(RoleNames.Admin))
            {
                return RoleNames.Admin;
            }
            if (claimsPrincipal.IsInRole(RoleNames.User))
            {
                return RoleNames.User;
            }
            return "";
        }
        
    }
}