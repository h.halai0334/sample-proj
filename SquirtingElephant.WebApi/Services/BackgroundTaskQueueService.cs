﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SquirtingElephant.Application.Interfaces;

namespace SquirtingElephant.WebApi.Services
{
    public class BackgroundTaskQueueService : IBackgroundTaskQueueService
    {
        private readonly ConcurrentQueue<(IRequest<object>,CancellationToken)> _workItems =
            new ConcurrentQueue<(IRequest<object>,CancellationToken)>();

        private SemaphoreSlim _signal = new SemaphoreSlim(0);

        public void QueueBackgroundWorkItem(IRequest<object> workItem)
        {
            QueueBackgroundWorkItem(workItem, CancellationToken.None);
        }

        public void QueueBackgroundWorkItem(IRequest<object> workItem, CancellationToken token)
        {
            if (workItem == null)
            {
                throw new ArgumentNullException(nameof(workItem));
            }

            _workItems.Enqueue((workItem,token));
            _signal.Release();
        }

        public async Task<IRequest<object>> DequeueAsync( CancellationToken cancellationToken)
        {
            await _signal.WaitAsync(cancellationToken);
            _workItems.TryDequeue(out var workItem);
            return workItem.Item1;
        }
    }
}